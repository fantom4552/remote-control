const ipc = require('electron').ipcRenderer;

document.getElementById('settingsClose').addEventListener('click', function (e) {
	e.preventDefault();
	ipc.send('settings-close');
});

function typeChanged(type) {
	switch (type) {
		case 'ftp': {
			document.getElementById('settingsFTP').style.display = 'block';
			document.getElementById('settingsSFTP').style.display = 'none';
		} break;

		case 'sftp': {
			document.getElementById('settingsFTP').style.display = 'none';
			document.getElementById('settingsSFTP').style.display = 'block';
		} break;

		case 'file': {
            document.getElementById('settingsFTP').style.display = 'none';
            document.getElementById('settingsSFTP').style.display = 'none';
		}

		default: {
			document.getElementById('settingsFTP').style.display = 'none';
			document.getElementById('settingsSFTP').style.display = 'none';
		}
	}
}

document.getElementById('settingsType').addEventListener('change', function (e) {
	e.preventDefault();
	typeChanged(this.value);
});

document.getElementById('settingsForm').addEventListener('submit', function (e) {
	e.preventDefault();

	const formData = new FormData(this);

	const settings = {};
	formData.forEach(function(value, key){
		settings[key] = value;
	});
	ipc.send('settings-save', settings);
});


ipc.on('settingsLoad', function(event, settings) {
	Object.keys(settings).forEach(function (key) {
		let el;
		if (key === 'type') {
			el = document.getElementById('settingsType');
		} else {
			el = document.querySelector('#settingsForm input[name="' + key + '"]');
		}

		if (el) {
			el.value = settings[key];
		}
	});

	typeChanged(settings.type);
});
