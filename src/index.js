const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');
const ipc = require('electron').ipcMain;
const settings = require('electron-settings');

const lodash = require('lodash');

const FtpTransport = require('./transports/ftp.js');
const SftpTransport = require('./transports/sftp.js');
const FileTransport = require('./transports/file.js');
const teamcityClient = require('./modules/teamcity.js');
const teamcityConfig = require('./modules/teamcity_cfg.js');
const Utils = require('./modules/utils.js');

let window, settingsWindow = null;

function openSettings() {
	if (settingsWindow !== null) {
		return;
	}

	settingsWindow = new BrowserWindow({
		parent: window,
		modal: true,
		show: false,
		width: 800,
		height: 600,
	});

	settingsWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'settings.html'),
		protocol: 'file:',
		slashes: true
	}));

	settingsWindow.once('ready-to-show', () => {
		// settingsWindow.webContents.openDevTools();
		settingsWindow.show();
		settingsWindow.webContents.send('settingsLoad', settings.get('settings'));
	});
}

function closeSettings() {
	if (settingsWindow === null) {
		return;
	}

	settingsWindow.close();
	settingsWindow = null;
}

function createWindow () {
	window = new BrowserWindow({
		width: 800,
		height: 400,
        icon: path.join(__dirname, 'assets/icons/icon.png')
	});

	window.loadURL(url.format({
		pathname: path.join(__dirname, 'index.html'),
		protocol: 'file:',
		slashes: true
	}));

	// window.webContents.openDevTools();
	window.on('closed', () => {
		window = null;
		settingsWindow = null;
	});

	if (!settings.has('settings')) {
		openSettings();
	}
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
});

app.on('activate', () => {
	if (window === null) {
		createWindow()
	}
});

ipc.on('settings-open', () => {
	openSettings();
});

ipc.on('settings-close', () => {
	closeSettings();
});

ipc.on('settings-save', (event, settingsData) => {
	settings.set('settings', settingsData);
	closeSettings();
});

ipc.on('download', async (event, config) => {
	try {
		window.webContents.send('status', 'Connecting to FTP');
		const settingsData = settings.get('settings');

		let transport;
		switch (settingsData.type) {
			case 'ftp': {
				transport = new FtpTransport(true);
				await transport.connect({
					host: settingsData.ftp_host,
					port: settingsData.ftp_port,
					user: settingsData.ftp_user,
					password: settingsData.ftp_password
				});
			} break;

			case 'sftp': {
				transport = new SftpTransport();
				await transport.connect({
					host: settingsData.sftp_host,
					port: settingsData.sftp_port,
					username: settingsData.sftp_user,
					password: settingsData.sftp_password
				});
			} break;

			case 'file': {
				transport = new FileTransport();
			} break;

			default: {
				throw new Error(`Bad type: ${settingsData.type}`);
			}
		}

		await transport.cwd(settingsData.dest);

		const directories = [];
		['rehlds', 'regame', 'metamod', 'reapi', 'revoice'].forEach(el => {
			if (config[el]) {
                teamcityConfig[el][settingsData.os].CREATE_DIRS.forEach(
                	dir => directories.push(dir)
                );
			}
		});

        await Utils.promiseArray(
        	lodash.uniq(directories),
            dir => transport.mkdir(dir, true)
		);

		const promises = ['rehlds', 'regame', 'metamod', 'reapi', 'revoice'].map(el => {
			if (!config[el]) {
				return undefined;
			}

			return teamcityClient.downloadFromTeamCity(transport, window.webContents, settingsData.os, el)
		});

		await Promise.all(promises);

		window.webContents.send('downloaded');
		window.webContents.send('status', 'Done!');
	} catch(err) {
		console.error(err);
		window.webContents.send('downloaded');
		window.webContents.send('status', 'Error!');
	}
});
