const ipc = require('electron').ipcRenderer;

const downloadStatus = document.getElementById('downloadStatus');
const submitBtn = document.getElementById('submitBtn');
let spinner;

ipc.on('status', function(event, message) {
    downloadStatus.innerHTML = message;
});

ipc.on('downloaded', function() {
    spinner.stop();
    submitBtn.disabled = false;
});

document.getElementById('settingsBtn').addEventListener('click', function (e) {
	e.preventDefault();
	ipc.send('settings-open');
});

document.getElementById('downloadForm').addEventListener('submit', function (e) {
	e.preventDefault();

    const formData = new FormData(this);

    const config = {};
    formData.forEach(function(value, key){
        config[key] = value;
    });
	ipc.send('download', config);
    submitBtn.disabled = true;
    spinner = new Spinner().spin();
    document.body.appendChild(spinner.el);
});
