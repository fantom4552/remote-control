const download = require('download');
const decompress = require('decompress');

const getFilesFromDecompressArray = require('./utils').getFilesFromDecompressArray;
const teamcityCfg = require('./teamcity_cfg');

const downloadFromTeamCity = async (transport, ipc, os, type) => {
	const {TITLE, DOWNLOAD_URL} = teamcityCfg[type];
	const UPLOAD_FILES = teamcityCfg[type][os].UPLOAD_FILES;

	let files;

	ipc.send('status', `${TITLE}: Downloading`);
	files = await download(DOWNLOAD_URL, {
		filename: `${type}.zip`,
		extract: true,
	});

	ipc.send('status', `${TITLE}: Extracting`);
	files = await decompress(files[0].data);

	ipc.send('status', `${TITLE}: Uploading`);
	const promises = UPLOAD_FILES.map(
        dstFile => getFilesFromDecompressArray(files, dstFile.src).map(
            srcFile => transport.upload(srcFile.data, dstFile.dst)
		)
	);

	await Promise.all(promises);
	ipc.send('status', `${TITLE}: Done`);
};

module.exports = {
	downloadFromTeamCity,
};
