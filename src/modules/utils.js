function getFilesFromDecompressArray(files, path) {
	const func = (typeof path === 'object' && path instanceof RegExp) ?
		function (element) {
			return path.test(element.path);
		} : function (element) {
			return element.path === path;
		};
	return files.filter(func);
}

function promiseArray(array, fn) {
    let index = 0;

    return new Promise(function(resolve, reject) {
        function next() {
            if (index < array.length) {
                fn(array[index++]).then(next, reject);
            } else {
                resolve();
            }
        }
        next();
    });
}

module.exports = {
	getFilesFromDecompressArray,
    promiseArray,
};
