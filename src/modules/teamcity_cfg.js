const REHLDS_TITLE = 'ReHLDS';
const REHLDS_DOWNLOAD_URL = 'http://teamcity.rehlds.org/guestAuth/downloadArtifacts.html?buildTypeId=Rehlds_Publish&buildId=lastSuccessful';
const REHLDS_CREATE_DIRS = {
	linux: [],
	windows: [],
};
const REHLDS_UPLOAD_FILES = {
	linux: [{
		src: 'bin/linux32/engine_i486.so',
		dst: 'engine_i486.so'
	}],
	windows: [{
		src: 'bin/win32/swds.dll',
		dst: 'swds.dll'
	}]
};

const REGAME_TITLE = 'ReGameDLL';
const REGAME_DOWNLOAD_URL = 'http://teamcity.rehlds.org/guestAuth/downloadArtifacts.html?buildTypeId=ReGameDLLCs_Publish&buildId=lastSuccessful';
const REGAME_CREATE_DIRS = {
	linux: ['cstrike', 'cstrike/dlls'],
	windows: ['cstrike', 'cstrike/dlls']
};
const REGAME_UPLOAD_FILES = {
	linux: [{
		src: 'bin/bugfixed/cs.so',
		dst: 'cstrike/dlls/cs.so'
	}],
	windows: [{
		src: 'bin/bugfixed/mp.dll',
		dst: 'cstrike/dlls/mp.dll'
	}, {
		src: 'bin/bugfixed/mp.pdb',
		dst: 'cstrike/dlls/mp.pdb'
	}]
};

const METAMOD_TITLE = 'Metamod-R';
const METAMOD_DOWNLOAD_URL = 'http://teamcity.rehlds.org/guestAuth/downloadArtifacts.html?buildTypeId=Metamod_Publish&buildId=lastSuccessful';
const METAMOD_CREATE_DIRS = {
	linux: ['cstrike', 'cstrike/addons', 'cstrike/addons/metamod'],
	windows: ['cstrike', 'cstrike/addons', 'cstrike/addons/metamod']
};
const METAMOD_UPLOAD_FILES = {
	linux: [{
		src: 'addons/metamod/metamod_i386.so',
		dst: 'cstrike/addons/metamod/metamod_i386.so'
	}],
	windows: [{
		src: 'addons/metamod/metamod.dll',
		dst: 'cstrike/addons/metamod/metamod.dll'
	}]
};

const REAPI_TITLE = 'ReAPI';
const REAPI_DOWNLOAD_URL = 'http://teamcity.rehlds.org/guestAuth/downloadArtifacts.html?buildTypeId=Reapi_Publish&buildId=lastSuccessful';
const REAPI_CREATE_DIRS = {
	linux: ['cstrike', 'cstrike/addons', 'cstrike/addons/amxmodx', 'cstrike/addons/amxmodx/modules'],
	windows: ['cstrike', 'cstrike/addons', 'cstrike/addons/amxmodx', 'cstrike/addons/amxmodx/modules']
};
const REAPI_UPLOAD_FILES = {
	linux: [{
		src: 'addons/amxmodx/modules/reapi_amxx_i386.so',
		dst: 'cstrike/addons/amxmodx/modules/reapi_amxx_i386.so'
	}],
	windows: [{
		src: 'addons/amxmodx/modules/reapi_amxx.dll',
		dst: 'cstrike/addons/amxmodx/modules//reapi_amxx.dll'
	}]
};

const REVOICE_TITLE = 'Revoice';
const REVOICE_DOWNLOAD_URL = 'http://teamcity.rehlds.org/guestAuth/downloadArtifacts.html?buildTypeId=Revoice_Publish&buildId=lastSuccessful';
const REVOICE_CREATE_DIRS = {
	linux: ['cstrike', 'cstrike/addons', 'cstrike/addons/revoice'],
	windows: ['cstrike', 'cstrike/addons', 'cstrike/addons/revoice']
};
const REVOICE_UPLOAD_FILES = {
	linux: [{
		src: 'bin/linux32/revoice_mm_i386.so',
		dst: 'cstrike/addons/revoice/revoice_mm_i386.so'
	}],
	windows: [{
		src: 'bin/win32/revoice_mm.dll',
		dst: 'cstrike/addons/revoice//revoice_mm.dll'
	}]
};

module.exports = {
	rehlds: {
		TITLE: REHLDS_TITLE,
		DOWNLOAD_URL: REHLDS_DOWNLOAD_URL,
		linux: {
			CREATE_DIRS: REHLDS_CREATE_DIRS.linux,
			UPLOAD_FILES: REHLDS_UPLOAD_FILES.linux
		},
		windows: {
			CREATE_DIRS: REHLDS_CREATE_DIRS.windows,
			UPLOAD_FILES: REHLDS_UPLOAD_FILES.windows
		}
	},
	regame: {
		TITLE: REGAME_TITLE,
		DOWNLOAD_URL: REGAME_DOWNLOAD_URL,
		linux: {
			CREATE_DIRS: REGAME_CREATE_DIRS.linux,
			UPLOAD_FILES: REGAME_UPLOAD_FILES.linux
		},
		windows: {
			CREATE_DIRS: REGAME_CREATE_DIRS.windows,
			UPLOAD_FILES: REGAME_UPLOAD_FILES.windows
		}
	},
	metamod: {
		TITLE: METAMOD_TITLE,
		DOWNLOAD_URL: METAMOD_DOWNLOAD_URL,
		linux: {
			CREATE_DIRS: METAMOD_CREATE_DIRS.linux,
			UPLOAD_FILES: METAMOD_UPLOAD_FILES.linux
		},
		windows: {
			CREATE_DIRS: METAMOD_CREATE_DIRS.windows,
			UPLOAD_FILES: METAMOD_UPLOAD_FILES.windows
		}
	},
	reapi: {
		TITLE: REAPI_TITLE,
		DOWNLOAD_URL: REAPI_DOWNLOAD_URL,
		linux: {
			CREATE_DIRS: REAPI_CREATE_DIRS.linux,
			UPLOAD_FILES: REAPI_UPLOAD_FILES.linux
		},
		windows: {
			CREATE_DIRS: REAPI_CREATE_DIRS.windows,
			UPLOAD_FILES: REAPI_UPLOAD_FILES.windows
		}
	},
	revoice: {
		TITLE: REVOICE_TITLE,
		DOWNLOAD_URL: REVOICE_DOWNLOAD_URL,
		linux: {
			CREATE_DIRS: REVOICE_CREATE_DIRS.linux,
			UPLOAD_FILES: REVOICE_UPLOAD_FILES.linux
		},
		windows: {
			CREATE_DIRS: REVOICE_CREATE_DIRS.windows,
			UPLOAD_FILES: REVOICE_UPLOAD_FILES.windows
		}
	}
};
