const ftp = require('ftp');

class FtpTransport {
	constructor(binary = false) {
		this.binary = binary;
		this.client = null;
	}

	connect(options) {
		const self = this;
		return new Promise((resolve, reject) => {
			const client = new ftp();
			client.on('ready', () => {
				if (!self.binary) {
					self.client = client;
					resolve(self);
				} else {
					client.binary(error => {
						if (error) {
							reject(error);
						} else {
							self.client = client;
							resolve(self);
						}
					})
				}
			});

			client.on('error', error => {
				this.client = null;
				reject(error);
			});

			client.connect(options);
		});
	}

	close() {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const client = this.client;
		return new Promise((resolve, reject) => {
			client.end();
			resolve();
		});
	}

	cwd(path) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const client = this.client;
		return new Promise((resolve, reject) => {
			client.cwd(path, (error) => {
				if (error) {
					reject(error);
				} else {
					resolve();
				}
			});
		});
	}

	download(source) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const client = this.client;
		return new Promise((resolve, reject) => {
			client.get(source, (error, data) => {
				if (error) {
					reject(error);
				} else {
					resolve(data);
				}
			});
		});
	}

	upload(data, dst) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const client = this.client;
		return new Promise((resolve, reject) => {
			client.put(data, dst, (error) => {
				if (error) {
					reject(error);
				} else {
					resolve();
				}
			});
		});
	}

    mkdir(path, recursive = false) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
        const client = this.client;
        return new Promise((resolve, reject) => {
            client.mkdir(path, recursive, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
	}

	chmod(mod, path) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const client = this.client;
		return new Promise((resolve, reject) => {
			client.site(`CHMOD ${mod} ${path}`, (error) => {
				if (error) {
					reject(error);
				} else {
					resolve();
				}
			});
		});
	}

    system() {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
        const client = this.client;
        return new Promise((resolve, reject) => {
            client.system((error, os) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(os);
                }
            });
        });
	}
}

module.exports = FtpTransport;
