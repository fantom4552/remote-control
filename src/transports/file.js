const fs = require('fs');

class FileTransport {
	constructor() {
		this.path = '';
	}

	connect(options) {
		return Promise.resolve(this);
	}

	close() {
		return Promise.resolve(this);
	}

	cwd(path) {
		this.path = path + '/';
		return Promise.resolve(this);
	}

	download(source) {
		const self = this;
		return new Promise((resolve, reject) => {
			fs.readFile(self.path + source, (error, data) => {
				if (error) {
					reject(error);
				} else {
					resolve(data);
				}
			});
		});
	}

	upload(data, dst) {
		const self = this;
		return new Promise((resolve, reject) => {
			fs.writeFile(self.path + dst, data, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
			});
		});
	}

    mkdir(path, recursive = false) {
        const self = this;
        return new Promise((resolve, reject) => {
			fs.mkdir(self.path + path, (error) => {
                if (error && error.code !== 'EEXIST') {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
	}

	chmod(mod, path) {
		return Promise.resolve(); // TODO: implement
	}

    system() {
        return Promise.resolve(); // TODO: implement
	}
}

module.exports = FileTransport;
