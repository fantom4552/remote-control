const Client = require('ssh2').Client;

class SftpTransport {
	constructor() {
		this.client = null;
		this.sftp = null;
		this.path = '';
	}

	connect(options) {
		const self = this;
		return new Promise((resolve, reject) => {
			const client = new Client();
			client.on('ready', () => {
				client.sftp((error, sftp) => {
					if (error) {
						self.client = null;
						reject(error);
					} else {
						self.client = client;
						self.sftp = sftp;
						resolve(self);
					}
				});
			}).connect(options);
		});
	}

	close() {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const client = this.client;
		return new Promise((resolve, reject) => {
			client.end();
			resolve();
		});
	}

	cwd(path) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		this.path = path + '/';
		return Promise.resolve();
	}

	download(source) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const self = this;
		return new Promise((resolve, reject) => {
			const stream = self.sftp.createReadStream(self.path + source);
			const bufs = [];
			stream.on('data', data => bufs.push(data));
			stream.on('end', () => resolve(Buffer.concat(bufs)));
			stream.on('error', error => reject(error));
		});
	}

	upload(data, dst) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
		const self = this;
		return new Promise((resolve, reject) => {
			const stream = self.sftp.createWriteStream(self.path + dst);
			stream.on('close', () => resolve());
			stream.on('end', () => resolve());
			stream.on('error', error => reject(error));
			stream.write(data);
			stream.end();
		});
	}

    mkdir(path, recursive = false) {
		if (this.client === null) {
			return Promise.reject(new Error('Client not connected'));
		}
        const self = this;
        return new Promise((resolve, reject) => {
			self.sftp.mkdir(self.path + path, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
	}

	chmod(mod, path) {
        return Promise.resolve(); // TODO: implement
	}

    system() {
        return Promise.resolve(); // TODO: implement
	}
}

module.exports = SftpTransport;
